
package com.client.service;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.client.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCustomerDetails_QNAME = new QName("http://service.client.com/", "getCustomerDetails");
    private final static QName _SetCustomerdetails_QNAME = new QName("http://service.client.com/", "setCustomerdetails");
    private final static QName _GetAllCustomers_QNAME = new QName("http://service.client.com/", "getAllCustomers");
    private final static QName _GetAllCustomersResponse_QNAME = new QName("http://service.client.com/", "getAllCustomersResponse");
    private final static QName _GetCustomerDetailsResponse_QNAME = new QName("http://service.client.com/", "getCustomerDetailsResponse");
    private final static QName _Customer_QNAME = new QName("http://service.client.com/", "customer");
    private final static QName _SetCustomerdetailsResponse_QNAME = new QName("http://service.client.com/", "setCustomerdetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.client.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAllCustomersResponse }
     * 
     */
    public GetAllCustomersResponse createGetAllCustomersResponse() {
        return new GetAllCustomersResponse();
    }

    /**
     * Create an instance of {@link GetCustomerDetailsResponse }
     * 
     */
    public GetCustomerDetailsResponse createGetCustomerDetailsResponse() {
        return new GetCustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link GetAllCustomers }
     * 
     */
    public GetAllCustomers createGetAllCustomers() {
        return new GetAllCustomers();
    }

    /**
     * Create an instance of {@link SetCustomerdetails }
     * 
     */
    public SetCustomerdetails createSetCustomerdetails() {
        return new SetCustomerdetails();
    }

    /**
     * Create an instance of {@link GetCustomerDetails }
     * 
     */
    public GetCustomerDetails createGetCustomerDetails() {
        return new GetCustomerDetails();
    }

    /**
     * Create an instance of {@link Customer1 }
     * 
     */
    public Customer1 createCustomer1() {
        return new Customer1();
    }

    /**
     * Create an instance of {@link SetCustomerdetailsResponse }
     * 
     */
    public SetCustomerdetailsResponse createSetCustomerdetailsResponse() {
        return new SetCustomerdetailsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "getCustomerDetails")
    public JAXBElement<GetCustomerDetails> createGetCustomerDetails(GetCustomerDetails value) {
        return new JAXBElement<GetCustomerDetails>(_GetCustomerDetails_QNAME, GetCustomerDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCustomerdetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "setCustomerdetails")
    public JAXBElement<SetCustomerdetails> createSetCustomerdetails(SetCustomerdetails value) {
        return new JAXBElement<SetCustomerdetails>(_SetCustomerdetails_QNAME, SetCustomerdetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllCustomers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "getAllCustomers")
    public JAXBElement<GetAllCustomers> createGetAllCustomers(GetAllCustomers value) {
        return new JAXBElement<GetAllCustomers>(_GetAllCustomers_QNAME, GetAllCustomers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllCustomersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "getAllCustomersResponse")
    public JAXBElement<GetAllCustomersResponse> createGetAllCustomersResponse(GetAllCustomersResponse value) {
        return new JAXBElement<GetAllCustomersResponse>(_GetAllCustomersResponse_QNAME, GetAllCustomersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCustomerDetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "getCustomerDetailsResponse")
    public JAXBElement<GetCustomerDetailsResponse> createGetCustomerDetailsResponse(GetCustomerDetailsResponse value) {
        return new JAXBElement<GetCustomerDetailsResponse>(_GetCustomerDetailsResponse_QNAME, GetCustomerDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Customer1 }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "customer")
    public JAXBElement<Customer1> createCustomer(Customer1 value) {
        return new JAXBElement<Customer1>(_Customer_QNAME, Customer1 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetCustomerdetailsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.client.com/", name = "setCustomerdetailsResponse")
    public JAXBElement<SetCustomerdetailsResponse> createSetCustomerdetailsResponse(SetCustomerdetailsResponse value) {
        return new JAXBElement<SetCustomerdetailsResponse>(_SetCustomerdetailsResponse_QNAME, SetCustomerdetailsResponse.class, null, value);
    }

}
