package com.project.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.client.service.Customer1;
import com.client.service.CustomerServiceImpl;
import com.client.service.CustomerServiceImplService;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String mobileNum = request.getParameter("mobilenum");
		String password = request.getParameter("pwd");
		CustomerServiceImpl webService = new CustomerServiceImplService().getCustomerServiceImplPort();
		Customer1 customer = webService.getCustomerDetails(Long.valueOf(mobileNum));
		if(customer!=null) {
		String name = customer.getFirstName()+" "+customer.getLastName();
		String mobileNumber = String.valueOf(customer.getMobileNumber());
		RequestDispatcher dispatcher = request.getRequestDispatcher("SelectPlan.jsp");
		request.setAttribute("name",name);
		request.setAttribute("mobile", mobileNum);
		dispatcher.forward(request, response);
		
		}
		else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("Error.jsp");
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
