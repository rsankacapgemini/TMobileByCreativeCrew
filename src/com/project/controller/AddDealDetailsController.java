package com.project.controller;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.DealDetails;

/**
 * Servlet implementation class AddDealDetails
 */
@WebServlet("/AddDealDetailsController")
public class AddDealDetailsController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddDealDetailsController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// response.getWriter().append("Served at: ").append(request.getContextPath());

		/*
		 * String REST_SERVICE_URL =
		 * "http://localhost:2027/Deals_Rest/dealDetails/deals/"; Client client =
		 * ClientBuilder.newClient();
		 * 
		 * String dealName = request.getParameter("dealName"); String dealDesc =
		 * request.getParameter("dealDesc"); Form form = new Form();
		 * form.param("dealName", dealName); form.param("dealDesc", dealDesc);
		 * 
		 * String callResult = client .target(REST_SERVICE_URL)
		 * .request(MediaType.APPLICATION_JSON) .post(Entity.entity(form,
		 * MediaType.APPLICATION_FORM_URLENCODED_TYPE), String.class);
		 */
		String dealName = request.getParameter("dealName");
		String dealDesc = request.getParameter("dealDesc");
		Form form = new Form();
		form.param("dealName", dealName);
		form.param("dealDesc", dealDesc);
		Client client = ClientBuilder.newClient();
		ObjectMapper objectMapper = new ObjectMapper();
		WebTarget webTarget = client.target("http://localhost:8085/Deals_Rest/dealDetails/deals");
		Invocation.Builder inBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		String responseFromRest = inBuilder.post(Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE),
				String.class);
		TypeReference<List<DealDetails>> mapType = new TypeReference<List<DealDetails>>() {
		};
		List<DealDetails> dealsList = objectMapper.readValue(responseFromRest, mapType);
		request.setAttribute("dealsList", dealsList);
		RequestDispatcher dispatcher = request.getRequestDispatcher("DisplayAllDeals.jsp");
		dispatcher.forward(request, response);

		/*
		 * PrintWriter out = response.getWriter(); out.print(callResult);
		 */
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}


}
