package com.project.controller;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.project.model.PlanDetails;

/**
 * Servlet implementation class DisplayPlanController
 */
@WebServlet("/DisplayPlanController")
public class DisplayPlanController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DisplayPlanController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient();

		String mobileNumberFromResponse = request.getParameter("MobileNum");
		String mobileNumber = mobileNumberFromResponse.substring(0,mobileNumberFromResponse.length()-1);
		String name = null;
		if (request.getParameter("4Lines") != null) {
			System.out.println(mobileNumber);
			name = "4 lines";
		} else if (request.getParameter("onePlus") != null) {
			name = "T-Mobile ONE plus";

		} else if (request.getParameter("onePlusInternational") != null) {
			name = "T-Mobile ONE plus International";

		} else if (request.getParameter("unlimited55+") != null) {
			name = "T-Mobile One Unlimited 55+";
		}
		WebTarget webTarget = client.target("http://localhost:8085/T-Mobile_Plans/TMobileService/TMobilePlans/")
				.path(name);
		Invocation.Builder inBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		String responseFromRest = inBuilder.get(String.class);
		System.out.println(responseFromRest);
	
		ObjectMapper mapper = new ObjectMapper();
		PlanDetails planDetails = mapper.readValue(responseFromRest,PlanDetails.class);
		RequestDispatcher dispatcher = request.getRequestDispatcher("PlanDescription.jsp");
		request.setAttribute("mobileNumber", mobileNumber);
		request.setAttribute("planDetails", planDetails);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
