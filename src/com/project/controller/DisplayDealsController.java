package com.project.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import javax.json.JsonObject;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.json.JSONArray;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.model.DealDetails;

/**
 * Servlet implementation class DisplayDealsController
 */
@WebServlet("/DisplayDealsController")
public class DisplayDealsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DisplayDealsController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Client client = ClientBuilder.newClient();
    	ObjectMapper objectMapper = new ObjectMapper();
		WebTarget webTarget = client.target("http://localhost:8085/Deals_Rest/dealDetails/deals");
		Invocation.Builder inBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		String responseFromRest = inBuilder.get(String.class);
    	TypeReference<List<DealDetails>> mapType = new TypeReference<List<DealDetails>>() {};
    	List<DealDetails> dealsList = objectMapper.readValue(responseFromRest, mapType);
    	RequestDispatcher dispatcher = request.getRequestDispatcher("DisplayAllDeals.jsp");
		request.setAttribute("dealsList", dealsList);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
