package com.project.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.client.service.Customer1;
import com.client.service.CustomerServiceImpl;
import com.client.service.CustomerServiceImplService;

/**
 * Servlet implementation class RegistrationController
 */
@WebServlet("/RegistrationController")
public class RegistrationController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistrationController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("fname");
		String lastName = request.getParameter("lname");
		String mobileNum = request.getParameter("mobilenum");
		String dob = request.getParameter("dob");
		Long mNum = Long.parseLong(mobileNum);
		String email = request.getParameter("emailId");
		String password = request.getParameter("pwd");
		Customer1 customer = new Customer1();
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setMailid(email);
		customer.setMobileNumber(mNum);
		customer.setPassword(password);
		customer.setDob(dob);
		CustomerServiceImpl webService = new CustomerServiceImplService().getCustomerServiceImplPort();
		webService.setCustomerdetails(customer);
		RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
		dispatcher.forward(request, response);


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
