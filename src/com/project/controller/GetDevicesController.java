package com.project.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.device.core.Device;
import com.device.core.DeviceInterfaceImplementation;
import com.device.core.DeviceInterfaceImplementationService;


/**
 * Servlet implementation class GetDevicesServlet
 */
@WebServlet("/GetDevicesController")
public class GetDevicesController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDevicesController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DeviceInterfaceImplementation webservice = new DeviceInterfaceImplementationService().getDeviceInterfaceImplementationPort();
		String stringdeviceID = request.getParameter("myDeviceId");
		Device selecteddevice = webservice.getByName(stringdeviceID);
		System.out.println(selecteddevice);
		RequestDispatcher dispatcher = null;
		if(selecteddevice.getDeviceName().equals("S8"))
			 dispatcher = request.getRequestDispatcher("ViewS8.jsp");
		else if(selecteddevice.getDeviceName().equals("IPhone X"))
			 dispatcher = request.getRequestDispatcher("ViewDevice.jsp");
		else if(selecteddevice.getDeviceName().equals("IPhone 8"))
			 dispatcher = request.getRequestDispatcher("ViewIPhone8.jsp");
		else if(selecteddevice.getDeviceName().equals("Pixel 2"))
			 dispatcher = request.getRequestDispatcher("ViewPixel.jsp");
		
		request.setAttribute("thedevice", selecteddevice);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
