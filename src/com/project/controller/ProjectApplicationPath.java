package com.project.controller;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/T-MobileOne")

public class ProjectApplicationPath extends Application {
	//it triggers the servlet scanning without any mapping in the web.xml
}
