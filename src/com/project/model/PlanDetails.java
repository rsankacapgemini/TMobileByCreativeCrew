/**
 * 
 */
package com.project.model;

import java.io.Serializable;

/**
 * @author gyelwand
 *
 */
public class PlanDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7142011643950882231L;
	
	private int planId;
	private String planName;
	private String planDescription;
	private int planCost;
	
	public PlanDetails(int planId, String planName, String planDescription, int planCost) 
			                       throws IllegalArgumentException{
		
		if(planName==null) {
			throw new IllegalArgumentException("Null Plan name is not allowed");
		}
		
		if(planCost<0) {
			throw new IllegalArgumentException("Negative plan cost is not allowed");
		}
		
		if(planDescription.length()<=0) {
			throw new IllegalArgumentException("Empty plan Description is not allowed");
		}
		this.planId=planId;
		this.planName=planName;
		this.planDescription=planDescription;
		this.planCost=planCost;
	}
	/**
	 * @return the planId
	 */
	public int getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the planDescription
	 */
	public String getPlanDescription() {
		return planDescription;
	}
	/**
	 * @param planDescription the planDescription to set
	 */
	public void setPlanDescription(String planDescription) {
		this.planDescription = planDescription;
	}
	/**
	 * @return the planCost
	 */
	public int getPlanCost() {
		return planCost;
	}
	/**
	 * @param planCost the planCost to set
	 */
	public void setPlanCost(int planCost) {
		this.planCost = planCost;
	}
	public PlanDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "PlanDetails [planId=" + planId + ", planName=" + planName + ", planDescription=" + planDescription
				+ ", planCost=" + planCost + "]";
	}
	
	
	

}
