package com.project.model;

public class DealDetails {

	private String dealName;
	private String dealDescription;
	
	public DealDetails(String dealName, String dealDescription) {
		this.dealName = dealName;
		this.dealDescription = dealDescription;
	}
	public String getDealName() {
		return dealName;
	}
	public void setDealName(String dealName) {
		this.dealName = dealName;
	}
	public String getDealDescription() {
		return dealDescription;
	}
	
	public void setDealDescription(String dealDescription) {
		this.dealDescription = dealDescription;
	}
	
	public DealDetails() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "DealDetails [dealName=" + dealName + ", dealDescription=" + dealDescription + "]";
	}
	
}
