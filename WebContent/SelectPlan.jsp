<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
 <%
String name = request.getAttribute("name").toString();
String mobileNumber = request.getAttribute("mobile").toString();
%>
<title>Hello<%=name %>! </title> 

</head>
<div>
    <img id="uxcMyImageId" src="./images/header_logo.jpg" width="1250px" height="150px" />
    </div>
    
    <h1><%=name %>!</h1>
    <h2> Please select from the below plans:</h2>
<body>
<form action="DisplayPlanController" method ="get" >

    <input type='hidden' id='mobileNum' name='MobileNum' value=<%=mobileNumber%>/>

	<table align="center">
	<tr class="table-primary">
		 <td>
		    <button type="submit" class="btn btn-primary btn-block" name="4Lines">4 lines</button>
		 </td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr class="table-secondary">
	 	<td>
		    <button type="submit" class="btn btn-primary btn-block" name="onePlus">T-Mobile ONE plus</button>
		 </td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>
	<tr class="table-success">
	 	<td>
	    	<button type="submit" class="btn btn-primary btn-block"name="onePlusInternational">T-Mobile ONE plus International<</button>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr><td>&nbsp;</td></tr>

	<tr class="table-danger">
		<td>
		   	<button type="submit" class="btn btn-primary btn-block" name="unlimited55+">T-Mobile One Unlimited 55+</button>
		</td>
	</tr>
	</table>
</form>

<!-- <form action="PlanDetailsService" method="get">
<select name="planDetails">
 <option value="4 lines">4 lines</option>
 <option value="T-Mobile ONE plus">T-Mobile ONE plus</option>
 <option value="T-Mobile ONE plus International">T-Mobile ONE plus International</option>
 <option value="T-Mobile One Unlimited 55+">T-Mobile One Unlimited 55+</option>
</select>
 <input type=submit name="selectplan" value="Select Plan"></input>
</form> -->

</body>
</html>