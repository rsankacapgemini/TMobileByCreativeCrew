    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <style>
    p {
        background-color: yellow;
    }
    </style>

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Insert title here</title>

    </head>
    <script>
    function validateform(){  
        var fname=document.registerUser.fname.value;  
        var lname=document.registerUser.lname.value; 
        var mobilenum=document.registerUser.mobilenum.value; 
        var emailId=document.registerUser.emailId.value; 
        var dob=document.registerUser.dob.value; 
        var password=document.registerUser.password.value; 
        var confirmPassword=document.registerUser.confirmPassword.value;

        if(fname==null || fname==""){
            alert("Please enter First Name");
            return false;
        }
        else if(lname==null || lname==""){
            alert("Please enter Last Name");
            return false;
        }
        else if(mobilenum==null || mobilenum==""){
            alert("Please enter Mobile Number");
            return false;
        }
        else if(emailId==null || emailId==""){
            alert("Please enter Email Id");
            return false;
        }
        else if(dob==null || dob==""){
            alert("Please enter Date Of Birth");
            return false;
        }
        else if(password==null || password==""){
            alert("Please enter Password");
            return false;
        }
        else if(confirmPassword==null || confirmPassword==""){
            alert("Please enter Confirm Password");
            return false;
        }
        else if(password!=confirmPassword){
            alert("Password and Confirm Password does not match");
            return false;
        }
      
        	return true;
       

    }
    </script>
    <body>

    <div>

    <img id="uxcMyImageId" src="./images/header_logo.jpg" width="1250px" height="150px" />
    </div>

    <form class="form-horizontal" name="registerUser" action="RegistrationController" method="get" onsubmit="return validateform()">

    <div align="left" style="color: red"><h3>&nbsp;&nbsp;&nbsp;&nbsp;Sign up for T-Mobile ID</h3></div> 
     <div class="form-group" align="center">
        <label for="firstName" class="col-sm-4 control-label">First Name</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="fname" name="fname" placeholder="First Name" required onclick="this.value=''">
        </div>
      </div>

       <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Last Name</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="lname" name="lname" placeholder="Last Name" required onclick="this.value=''">
        </div>
      </div> 

        <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Phone Number</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="mobilenum" name="mobilenum" placeholder="Mobile Number" required onclick="this.value=''">
        </div>
      </div>

       <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Email</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="emailId" name="emailId" placeholder="Email" required onclick="this.value=''">
        </div>
      </div>

        <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Date of Birth</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dob" name="dob" placeholder="Date of Birth" required onclick="this.value=''">
        </div>
      </div>


       <div class="form-group" align="center">
            <label for="lastName" class="col-sm-4 control-label">Password</label>
            <div class="col-sm-5" align="center">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required onclick="this.value=''">
            </div>
      </div>

       <div class="form-group" align="center">
            <label for="lastName" class="col-sm-4 control-label">Re-enter Password</label>
            <div class="col-sm-5" align="center">
            <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password" required onclick="this.value=''">
            </div>
      </div>

     <div class="form-group" align="center">

        <div class="col-sm-10" align="center">
         <button type="submit" class="btn btn-danger">Register</button>
        </div>
      </div>



    <!-- <input type="text" name="fname" value="First Name" onclick="this.value=''"/><br/>  
    <input type="text" name="lname" value="Last Name" onclick="this.value=''"/><br/>  
    <input type="text" name="mobilenum" value="Mobile Number" onclick="this.value=''"/></input><br/>  
    <input type="text" name="emailId" value="Email" onclick="this.value=''"/><br/>
    <input type="text" name="dob" value="Date of Birth" onclick="this.value=''"/><br/>
    <input type="password" name="pwd" value="password" onclick="this.value=''"/><br/>   -->
    <!-- <input type="submit" value="register"></input> -->
    </form>  
    </body>
    </html>