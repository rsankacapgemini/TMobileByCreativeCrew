    <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>
    <style>
    p {
        background-color: yellow;
    }
    </style>

    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Insert title here</title>

    </head>
    <script>
    function validateform(){  
        var fname=document.registerUser.fname.value;  
        var lname=document.registerUser.lname.value; 
        var mobilenum=document.registerUser.mobilenum.value; 
        var emailId=document.registerUser.emailId.value; 
        var dob=document.registerUser.dob.value; 
        var password=document.registerUser.password.value; 
        var confirmPassword=document.registerUser.confirmPassword.value;

        if(fname==null || fname==""){
            alert("Please enter First Name");
            return false;
        }
        else if(lname==null || lname==""){
            alert("Please enter Last Name");
            return false;
        }
        else if(mobilenum==null || mobilenum==""){
            alert("Please enter Mobile Number");
            return false;
        }
        else if(emailId==null || emailId==""){
            alert("Please enter Email Id");
            return false;
        }
        else if(dob==null || dob==""){
            alert("Please enter Date Of Birth");
            return false;
        }
        else if(password==null || password==""){
            alert("Please enter Password");
            return false;
        }
        else if(confirmPassword==null || confirmPassword==""){
            alert("Please enter Confirm Password");
            return false;
        }
        else if(password!=confirmPassword){
            alert("Password and Confirm Password does not match");
            return false;
        }
      
        	return true;
       

    }
    </script>
    <body>

    <div>

    <img id="uxcMyImageId" src="./images/header_logo.jpg" width="1250px" height="150px" />
    </div>

    <form class="form-horizontal" action="AddDealDetailsController" method="get" >

    <div align="left" style="color: red"><h3>&nbsp;&nbsp;&nbsp;&nbsp;Add a New Deal</h3></div> 
     <div class="form-group" align="center">
        <label for="firstName" class="col-sm-4 control-label">Deal Name</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dealName" name="dealName" placeholder="Deal Name" required onclick="this.value=''">
        </div>
      </div>

       <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Deal Description</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dealDesc" name="dealDesc" placeholder="Deal Name" required onclick="this.value=''">
        </div>
      </div> 
     <div class="form-group" align="center">

        <div class="col-sm-10" align="center">
        <input type=submit name="addDeal" class="btn btn-danger" value="Add Deal"></input>
        </div>
      </div>
      </form>

<form class="form-horizontal" action="UpdateDealDetailsController" method="get" >

    <div align="left" style="color: red"><h3>&nbsp;&nbsp;&nbsp;&nbsp;Update the Existing Deal</h3></div> 
     <div class="form-group" align="center">
        <label for="firstName" class="col-sm-4 control-label">Deal Name</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dealName" name="dealName" placeholder="Deal Name" required onclick="this.value=''">
        </div>
      </div>

       <div class="form-group" align="center">
        <label for="lastName" class="col-sm-4 control-label">Deal Description</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dealDesc" name="dealDesc" placeholder="Deal Name" required onclick="this.value=''">
        </div>
      </div> 
     <div class="form-group" align="center">

        <div class="col-sm-10" align="center">
        <input type=submit name="addDeal" class="btn btn-danger" value="Update Deal"></input>
        </div>
      </div>
      </form>
      
      <form class="form-horizontal" action="DeleteDealDetailsController" method="get" >

    <div align="left" style="color: red"><h3>&nbsp;&nbsp;&nbsp;&nbsp;Delete the Existing Deal</h3></div> 
     <div class="form-group" align="center">
        <label for="firstName" class="col-sm-4 control-label">Deal Name</label>
        <div class="col-sm-5" align="center">
        <input type="text" class="form-control" id="dealName" name="dealName" placeholder="Deal Name" required onclick="this.value=''">
        </div>
      </div>

       
     <div class="form-group" align="center">

        <div class="col-sm-10" align="center">
        <input type=submit name="addDeal" class="btn btn-danger" value="Delete Deal"></input>
        </div>
      </div>
      </form>



    <!-- <input type="text" name="fname" value="First Name" onclick="this.value=''"/><br/>  
    <input type="text" name="lname" value="Last Name" onclick="this.value=''"/><br/>  
    <input type="text" name="mobilenum" value="Mobile Number" onclick="this.value=''"/></input><br/>  
    <input type="text" name="emailId" value="Email" onclick="this.value=''"/><br/>
    <input type="text" name="dob" value="Date of Birth" onclick="this.value=''"/><br/>
    <input type="password" name="pwd" value="password" onclick="this.value=''"/><br/>   -->
    <!-- <input type="submit" value="register"></input> -->
    </form>  
    </body>
    </html>