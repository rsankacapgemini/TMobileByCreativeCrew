<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">


</style> 

<div>

<img id="uxcMyImageId" src="./images/header_logo.jpg" width="1300px" height="150px" />
</div>
<body>
	<h1>Our Deals are here</h1>
	<div align="center">
		<form action="">
			<table class="table table-striped table-hover" border="1">

				<thead>
					<tr class="bg-success">

						<th>Deal Name</th>
						<th>Deal Description</th>

					</tr>
				</thead>
				<c:forEach var="dealsList" items="${dealsList}">
				
				<tr>
					<td>${dealsList.dealName}</td>
					<td>${dealsList.dealDescription}</td>

				</tr>
		</c:forEach>
				

			</table>

			<br />

		</form>
		<br />

	</div>
</body>
</html>